package com.hfad.stopwatch;

import android.app.Activity;
import android.os.Handler;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import java.util.Locale;

public class StopwatchActivity extends Activity
{
    private int seconds = 0;
    private boolean running = false;
    private boolean previouslyRunning = false;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stopwatch);

        if(savedInstanceState != null)
        {
            seconds = savedInstanceState.getInt("seconds");
            running = savedInstanceState.getBoolean("running");
            previouslyRunning = savedInstanceState.getBoolean("previouslyRunning");
        }

        runTimer();
    }

    //region Stopwatch Buttons Functionality

    //Start the stopwatch when the start button is clicked
    public void onClickStart(View view)
    {
        running = true;
    }

    //Stop the stopwatch when the stop button is clicked
    public void onClickStop(View view)
    {
        running = false;
    }

    //Reset the stopwatch when the reset button is clicked
    public void onClickReset(View view)
    {
        running = false;
        seconds = 0;
    }

    //endregion


    //region Lifecycle Methods

    @Override
    protected void onPause()
    {
        super.onPause();
        previouslyRunning = running;
        running = false;
    }

    @Override
    protected void onResume()
    {
        super.onResume();

        if(previouslyRunning)
            running = true;
    }

    //endregion

    //Function loops around after each second (1000 ms)
    private void runTimer()
    {
        final TextView timeView = findViewById(R.id.stopwatchDisplay);
        final Handler handler = new Handler();

        handler.post(new Runnable()
        {
            @Override
            public void run()
            {
                int hours = seconds/3600;
                int minutes = (seconds%3600)/60;
                int secs = seconds%60;

                String time = String.format(Locale.getDefault(),
                        "%d:%02d:%02d", hours, minutes, secs);

                timeView.setText(time);

                if(running)
                {
                    seconds++;
                }

                handler.postDelayed(this, 1000);
            }
        });
    }

    //Overriding the class method to save the instance to preserve the stopwatch value
    //even when the device configuration changes : (eg. device is rotated)
    @Override
    public void onSaveInstanceState(Bundle savedInstanceState)
    {
        savedInstanceState.putInt("seconds", seconds);
        savedInstanceState.putBoolean("running", running);
        savedInstanceState.putBoolean("previouslyRunning", previouslyRunning);
    }
}
